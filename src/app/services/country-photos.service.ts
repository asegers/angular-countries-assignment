import {createApi} from 'unsplash-js';
import fetch from 'node-fetch';

export type ImgData = {
  alt: string
  src: string
};

export class CountryPhotosService {
  UnsplashApi = createApi({
    accessKey: 'LieHg7ehC9dJeX-jGmpYPfxiM15J8vpz0Mkv31No2D4',
    fetch
  });

  async getByCountry(country: string, page: number = 1, perPage: number = 10): Promise<ImgData[]> {
      const res = await this.UnsplashApi
        .search
        .getPhotos({
          query: country,
          page,
          perPage
        });
      if (res.response.total === 0) {
        throw Error('Could not find country');
      }
      return res.response.results.map(result => ({
        alt: result.description || result.alt_description,
        src: result.urls.regular
      }));
  }
}
