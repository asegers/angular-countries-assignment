import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {CountryPhotosService, ImgData} from '../../services/country-photos.service';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css'],
  providers: [CountryPhotosService]
})
export class CountryComponent implements OnInit {
  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public photosService: CountryPhotosService
  ) {}
  country: string;
  images: ImgData[] = null;
  ngOnInit(): void {
    this.route.params.subscribe(({country}: Params) => {
      this.country = country;
      this.photosService.getByCountry(country, Math.ceil(Math.random() * 12))
        .then(images => { this.images = images; })
        .catch(() => this.router.navigate(['404']));
    });
  }
}
