import {Component} from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  constructor(config: NgbCarouselConfig) {
    config.interval = 8000;
    config.wrap = true;
    config.keyboard = false;
    // config.pauseOnHover = false;
  }
  carouselImages = [
    {
      country: 'England',
      alt: 'Tower Bridge sunset',
      src: 'https://images.unsplash.com/photo-1543832923-44667a44c804?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjY0NzZ8MHwxfHNlYXJjaHw0fHxlbmdsYW5kfGVufDB8fHx8MTYxOTQ5NTE5MA&ixlib=rb-1.2.1&q=80&w=2250'
    },
    {
      country: 'France',
      alt: 'Eiffel Tower, Paris France',
      src: 'https://images.unsplash.com/photo-1502602898657-3e91760cbb34?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjY0NzZ8MHwxfHNlYXJjaHw0fHxmcmFuY2V8ZW58MHx8fHwxNjE5NDk1MTkw&ixlib=rb-1.2.1&q=80&w=2250'
    },
    {
      country: 'Greece',
      alt: 'Parthenon Temple at Athen, Greece',
      src: 'https://images.unsplash.com/photo-1503152394-c571994fd383?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjY0NzZ8MHwxfHNlYXJjaHw0fHxncmVlY2V8ZW58MHx8fHwxNjE5NDk1MTkw&ixlib=rb-1.2.1&q=80&w=2250'
    },
    {
      country: 'Spain',
      alt: 'empty seats and tables; in between; buildings; during; daytime;',
      src: 'https://images.unsplash.com/photo-1549643276-fdf2fab574f5?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjY0NzZ8MHwxfHNlYXJjaHw0fHxzcGFpbnxlbnwwfHx8fDE2MTk0OTUxOTA&ixlib=rb-1.2.1&q=80&w=2250'
    }
  ];
}
