import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent {
  accountForm = new FormGroup({
    username: new FormControl('', [
      Validators.required, Validators.minLength(4)
    ]),
    email: new FormControl('', [
      Validators.required, Validators.email
    ]),
    password: new FormControl('Password123!', [
      Validators.required,
      Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/)
    ]),
  });
  loading = false;
  hidePassword = true;
  onSubmit(): void {
    this.accountForm.disable();
    this.loading = true;
    setTimeout(() => {
      this.accountForm.enable();
      this.loading = false;
    }, 4000);
  }
}
