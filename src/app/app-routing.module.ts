import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountComponent} from './pages/account/account.component';
import {ContactComponent} from './pages/contact/contact.component';
import {CountriesComponent} from './pages/countries/countries.component';
import {CountryComponent} from './pages/country/country.component';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {HomeComponent} from './pages/home/home.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'account', component: AccountComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'countries', component: CountriesComponent},
  {path: 'countries/:country', component: CountryComponent},
  {path: '404', component: NotFoundComponent},
  {path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
